include(src/utils.m4)
int SUBFUNCTION(TYPE, compare)(const void *, const void *);
int SUBFUNCTION(TYPE, add)(struct TYPE *, int *, struct TYPE);
struct TYPE *SUBFUNCTION(TYPE, search)(struct TYPE *, int, char *);

int SUBFUNCTION(TYPE, compare)(const void *el1, const void *el2) 
{
	struct TYPE *TYPE1 = (struct TYPE *) el1;
	struct TYPE *TYPE2 = (struct TYPE *) el2;

	return strcmp(TYPE1->KEYNAME, TYPE2->KEYNAME);
}

int SUBFUNCTION(TYPE, add)(struct TYPE *array, int *n, struct TYPE el)
{
	array[*n] = el;

	*n = *n + 1;

	return 0;
}

struct TYPE *SUBFUNCTION(TYPE, search)(struct TYPE *array, int n, char *KEYNAME)
{
	int first=0, last=n, middle=n/2, res = 0;

	while (first <= last) {
		res = strcmp(array[middle].KEYNAME, KEYNAME);
		if (res == 0)
			return array + middle;
		else if (res < 0)
			first = middle + 1;
		else if (res > 0)
			last = middle - 1;

		middle = (first + last)/2;
	}

	return NULL;
}
